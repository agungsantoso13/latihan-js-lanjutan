// //function expression
// const tampilNama = function (nama) {
//     return `Halo, ${nama}`;
// }

// console.log(tampilNama('Agung Santoso'));

// //arrow function
// const tampilNama = (nama) => {
//     return `Halo, ${nama}`;
// }

// console.log(tampilNama('Aijaz Nuha'));


// const tampilNama = nama => {
//     return `Halo, ${nama}`;
// }

// console.log(tampilNama('Aijaz Nuha'));

// //implicit return
// const tampilNama = (nama, waktu) => `Selamat ${waktu}, ${nama}`;

// console.log(tampilNama('Tiara RA', 'Pagi'));

// const tampilNama = () => 'Hello World';
// console.log(tampilNama());


let mahasiswa = ['Agung Santoso', 'Aijaz Nuha', 'Tiara RA']

// let jumlahHuruf = mahasiswa.map(function (nama) {
//     return nama.length;
// }
// )
// console.log(jumlahHuruf);

// // ini mengembalikan array
// let jumlahHuruf = mahasiswa.map(nama => nama.length);
// console.log(jumlahHuruf);

//ini mengembalikan objek
let jumlahHuruf = mahasiswa.map(nama => ({ nama: nama, jmlHuruf: nama.length }));

console.table(jumlahHuruf);