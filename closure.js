// // coba ke-1 closure
// function init() {
//     let nama = "AgungS";
//     // let umur = 33
//     // function tampilNama() {
//     // function tampilNama(nama) {
//     return function (nama) {


//         // let nama = "santoso";
//         console.log(nama);
//         // console.log(umur);

//     }
//     // console.dir(tampilNama);
//     // tampilNama();
//     // return tampilNama;

// }
// // init();
// let panggilNama = init();
// panggilNama('santo');
// panggilNama('nuha');



// // coba ke-2 function factories
// function ucapkanSalam(waktu) {
//     return function (nama) {
//         console.log(`Selamat ${waktu} ${nama}, semoga harimu menyenangkan!`);
//     };

// };

// let selamatPagi = ucapkanSalam('Pagi');
// let selamatSiang = ucapkanSalam('Siang');
// let selamatMalam = ucapkanSalam('malam');

// // console.dir(selamatPagi);
// console.log(selamatPagi('AgungS'));

//coba ke-3 private variable

// let counter = 0;
// let add = function () {
//     return ++counter
// }
// let counter = 10;


// let add = function () {
//     let counter = 0;
//     return function () {
//         return ++counter;
//     };
// };

// let a = add();
// let counter = 100; //variable global
// console.log(a());
// console.log(a());
// console.log(a());

let add = (function () {
    let counter = 0;
    return function () {
        return ++counter;
    };
})();

console.log(add());
console.log(add());
console.log(add());
